/* ps4000a.h
 *
 * This is a driver for a PicoScope 4000A Series area detector.
 *
 * Author: Hinko Kocevar
 *         European Spallation Source ERIC
 *
 * Created: Dec 19, 2017
 *
 * Based on ps4000con.[ch] from picosdk-c-examples:
 * Copyright (C) 2009 - 2017 Pico Technology Ltd. See PICO_LICENSE.md file for terms.
 *
 * EPICS areaDetector driver:
 * Copyright (C) 2018 - 2020 European Spallation Source ERIC
 */

#include <epicsEvent.h>
#include <epicsTime.h>
#include "asynNDArrayDriver.h"

/* Definition of ps4000a driver routines on Linux */
#include <libps4000a-1.0/ps4000aApi.h>
#ifndef PICO_STATUS
#include <libps4000a-1.0/PicoStatus.h>
#endif

#define DRIVER_VERSION                  1
#define DRIVER_REVISION                 2
#define DRIVER_MODIFICATION             0

// unit parameters
#define PS4000AStatusString                                 "PS4000A_STATUS"
#define PS4000AStatusMessageString                          "PS4000A_STATUS_MESSAGE"
#define PS4000ASamplingFrequency_                           "PS4000A.SAMPLING_FREQ"
#define PS4000ATickValue_                                   "PS4000A.TICK_VALUE"
#define PS4000ATimeBase_                                    "PS4000A.TIME_BASE"
#define PS4000ANumPreTrigSamples_                           "PS4000A.NUM.PRE_TRIG.SAMPLES"
#define PS4000ANumPostTrigSamples_                          "PS4000A.NUM.POST_TRIG.SAMPLES"
#define PS4000ANumTotalSamples_                             "PS4000A.NUM.TOTAL.SAMPLES"
#define PS4000ANumMaxSamples_                               "PS4000A.NUM.MAX.SAMPLES"
#define PS4000AMaximumValue_                                "PS4000A.MAXIMUM.VALUE"
#define PS4000AMinimumValue_                                "PS4000A.MINIMUM.VALUE"
#define PS4000AMaximumSegments_                             "PS4000A.MAXIMUM.SEGMENTS"
#define PS4000ATrigMode_                                    "PS4000A.TRIG.MODE"
#define PS4000ATrigRepeat_                                  "PS4000A.TRIG.REPEAT"
#define PS4000ATrigEnabled_                                 "PS4000A.TRIG.ENABLED"
#define PS4000APulseWidthQualifierEnabled_                  "PS4000A.PWQ.ENABLED"
// channel parameters
#define PS4000AChName_                                      "PS4000A.CH.NAME"
#define PS4000AChEnabled_                                   "PS4000A.CH.ENABLED"
#define PS4000AChCoupling_                                  "PS4000A.CH.COUPLING"
#define PS4000AChRange_                                     "PS4000A.CH.RANGE"
#define PS4000AChConvert_                                   "PS4000A.CH.CONVERT"
#define PS4000AChAnalogOffset_                              "PS4000A.CH.ANALOG_OFFSET"
#define PS4000AChTrigUse_                                   "PS4000A.CH.TRIG.USE"
#define PS4000AChTrigSimpleThreshold_                       "PS4000A.CH.TRIG.SIMPLE.THRESHOLD"
#define PS4000AChTrigSimpleThresholdMv_                     "PS4000A.CH.TRIG.SIMPLE.THRESHOLD.MV"
#define PS4000AChTrigSimpleDirection_                       "PS4000A.CH.TRIG.SIMPLE.DIRECTION"
#define PS4000AChTrigSimpleDelay_                           "PS4000A.CH.TRIG.SIMPLE.DELAY"
#define PS4000AChTrigSimpleTimeout_                         "PS4000A.CH.TRIG.SIMPLE.TIMEOUT"
#define PS4000AChTrigAdvancedCondition_                     "PS4000A.CH.TRIG.ADVANCED.CONDITION"
#define PS4000AChTrigAdvancedDirection_                     "PS4000A.CH.TRIG.ADVANCED.DIRECTION"
#define PS4000AChTrigAdvancedThresholdUpper_                "PS4000A.CH.TRIG.ADVANCED.THRESHOLD.UPPER"
#define PS4000AChTrigAdvancedThresholdUpperHysteresis_      "PS4000A.CH.TRIG.ADVANCED.THRESHOLD.UPPER_HYSTERESIS"
#define PS4000AChTrigAdvancedThresholdLower_                "PS4000A.CH.TRIG.ADVANCED.THRESHOLD.LOWER"
#define PS4000AChTrigAdvancedThresholdLowerHysteresis_      "PS4000A.CH.TRIG.ADVANCED.THRESHOLD.LOWER_HYSTERESIS"
#define PS4000AChTrigAdvancedThresholdMode_                 "PS4000A.CH.TRIG.ADVANCED.THRESHOLD.MODE"
#define PS4000AChTrigAdvancedDelay_                         "PS4000A.CH.TRIG.ADVANCED.DELAY"
#define PS4000AChTrigAdvancedTimeout_                       "PS4000A.CH.TRIG.ADVANCED.TIMEOUT"
#define PS4000AChPulseWidthQualifierCondition_              "PS4000A.CH.PWQ.CONDITION"
#define PS4000AChPulseWidthQualifierDirection_              "PS4000A.CH.PWQ.DIRECTION"
#define PS4000AChPulseWidthQualifierLimitLower_             "PS4000A.CH.PWQ.LIMIT.LOWER"
#define PS4000AChPulseWidthQualifierLimitUpper_             "PS4000A.CH.PWQ.LIMIT.UPPER"
#define PS4000AChPulseWidthQualifierType_                   "PS4000A.CH.PWQ.TYPE"

#define MAX_CHANNELS                    PS4000A_MAX_CHANNELS
#define OCTO_SCOPE                      8
#define QUAD_SCOPE                      4
#define DUAL_SCOPE                      2

typedef enum
{
    MODEL_NONE      = 0,
    MODEL_PS4824    = 4824,
    MODEL_PS4225    = 4225,
    MODEL_PS4425    = 4425,
    MODEL_PS4444    = 4444
} MODEL_TYPE;

typedef enum
{
    SIGGEN_NONE     = 0,
    SIGGEN_FUNCTGEN = 1,
    SIGGEN_AWG      = 2
} SIGGEN_TYPE;

/** PicoScope PS4000A driver. */
class epicsShareClass PS4000A : public asynNDArrayDriver {
public:
    PS4000A(const char *portName, int numSamples, NDDataType_t dataType,
                   int maxBuffers, size_t maxMemory,
                   int priority, int stackSize);
    virtual ~PS4000A();

    /* These are the methods that we override from asynNDArrayDriver */
    virtual asynStatus writeInt32(asynUser *pasynUser, epicsInt32 value);
    virtual asynStatus writeFloat64(asynUser *pasynUser, epicsFloat64 value);
    virtual void report(FILE *fp, int details);
    void acqTask(); /**< Should be private, but gets called from C, so must be public */

protected:
    // unit parameters
    int P_Status;
    #define FIRST_PS4000A_PARAM P_Status
    int P_StatusMessage;
    int P_TimeBase;
    int P_SamplingFreq;
    int P_TickValue;
    int P_NumPreTrigSamples;
    int P_NumPostTrigSamples;
    int P_NumTotalSamples;
    int P_NumMaxSamples;
    int P_MaximumValue;
    int P_MinimumValue;
    int P_MaximumSegments;
    int P_TrigMode;
    int P_TrigRepeat;
    int P_TrigEnabled;
    int P_PulseWidthQualifierEnabled;

    // channel parameters
    int P_ChName;
    int P_ChEnabled;    // TODO: refactor to CtrlStat
    int P_ChCoupling;
    int P_ChRange;
    int P_ChConvert;
    int P_ChAnalogOffset;
    int P_ChTrigUse;
    int P_ChTrigSimpleThreshold;
    int P_ChTrigSimpleThresholdMv;
    int P_ChTrigSimpleDirection;
    int P_ChTrigSimpleDelay;
    int P_ChTrigSimpleTimeout;
    int P_ChTrigAdvancedCondition;
    int P_ChTrigAdvancedDirection;
    int P_ChTrigAdvancedThresholdUpper;
    int P_ChTrigAdvancedThresholdUpperHysteresis;
    int P_ChTrigAdvancedThresholdLower;
    int P_ChTrigAdvancedThresholdLowerHysteresis;
    int P_ChTrigAdvancedThresholdMode;
    int P_ChTrigAdvancedDelay;
    int P_ChTrigAdvancedTimeout;
    int P_ChPulseWidthQualifierCondition;
    int P_ChPulseWidthQualifierDirection;
    int P_ChPulseWidthQualifierLimitLower;
    int P_ChPulseWidthQualifierLimitUpper;
    int P_ChPulseWidthQualifierType;

private:
    /* These are the methods that are new to this class */
    void setAcquire(int value);
    void getInfo();
    int32_t mvToAdc(int32_t mv, int32_t ch);
    int32_t adcToMv(int32_t raw, int32_t ch);
    asynStatus setupBuffer(int ch, int numSamples);
    asynStatus setupChannel(int ch);
    asynStatus setupTrigger(int ch);
    asynStatus setupTriggerNone();
    asynStatus setupTriggerSimple(int ch, int trigUse);
    asynStatus setupTriggerAdvanced(int ch, int trigUse);
    asynStatus setupTimeBase();
    asynStatus setupDevice();
    asynStatus checkForDevice();
    int countEnabledChannels();
    asynStatus armDevice();
    asynStatus waitDevice();
    template <typename epicsType> asynStatus updateArraysT();
    asynStatus updateArrays();

    /* Our data */
    epicsEventId mStartEventId;
    epicsEventId mStopEventId;
    int mUniqueId;
    int mAcquiring;

    int16_t mHandle;
    int16_t *mBuffer[MAX_CHANNELS];
    size_t mBufferSize;
    MODEL_TYPE mModel;
    PS4000A_RANGE mFirstRange;
    PS4000A_RANGE mLastRange;
    int mSignalGenerator;
    bool mHasETS;
    int mChannelCount;
    int16_t mMaxADCValue;
    bool mDoDeviceSetup;
};

