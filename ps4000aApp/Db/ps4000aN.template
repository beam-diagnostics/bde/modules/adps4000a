#=================================================================#
# Template file: ps4000aN.template
# Database for the records specific to the single channel of PicoScope 4000A Series driver
# Hinko Kocevar
# Created: Dec 19, 2017

record(stringin, "$(P)$(R)ChannelNameR")
{
    field(DTYP, "asynOctetRead")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.NAME")
    field(VAL,  "Not set")
    field(SCAN, "I/O Intr")
}

record(bo, "$(P)$(R)Enabled")
{
    field(PINI, "YES")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.ENABLED")
    field(ZNAM, "No")
    field(ONAM, "Yes")
    info(autosaveFields, "VAL")
}

record(bi, "$(P)$(R)EnabledR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.ENABLED")
    field(ZNAM, "No")
    field(ONAM, "Yes")
    field(SCAN, "I/O Intr")
}

record(bo, "$(P)$(R)Coupling")
{
    field(PINI, "YES")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.COUPLING")
    field(ZNAM, "AC")
    field(ONAM, "DC")
    info(autosaveFields, "VAL")
}

record(bi, "$(P)$(R)CouplingR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.COUPLING")
    field(ZNAM, "AC")
    field(ONAM, "DC")
    field(SCAN, "I/O Intr")
}

record(mbbo, "$(P)$(R)Range")
{
    field(PINI, "YES")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.RANGE")
    field(ZRST, "10 mV")
    field(ZRVL, "0")
    field(ONST, "20 mV")
    field(ONVL, "1")
    field(TWST, "50 mV")
    field(TWVL, "2")
    field(THST, "100 mV")
    field(THVL, "3")
    field(FRST, "200 mV")
    field(FRVL, "4")
    field(FVST, "500 mV")
    field(FVVL, "5")
    field(SXST, "1 V")
    field(SXVL, "6")
    field(SVST, "2 V")
    field(SVVL, "7")
    field(EIST, "5 V")
    field(EIVL, "8")
    field(NIST, "10 V")
    field(NIVL, "9")
    field(TEST, "20 V")
    field(TEVL, "10")
    field(ELST, "50 V")
    field(ELVL, "11")
#    field(TVST, "100 V")
#    field(TVVL, "12")
    info(autosaveFields, "VAL")
}

record(mbbi, "$(P)$(R)RangeR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.RANGE")
    field(ZRST, "10 mV")
    field(ZRVL, "0")
    field(ONST, "20 mV")
    field(ONVL, "1")
    field(TWST, "50 mV")
    field(TWVL, "2")
    field(THST, "100 mV")
    field(THVL, "3")
    field(FRST, "200 mV")
    field(FRVL, "4")
    field(FVST, "500 mV")
    field(FVVL, "5")
    field(SXST, "1 V")
    field(SXVL, "6")
    field(SVST, "2 V")
    field(SVVL, "7")
    field(EIST, "5 V")
    field(EIVL, "8")
    field(NIST, "10 V")
    field(NIVL, "9")
    field(TEST, "20 V")
    field(TEVL, "10")
    field(ELST, "50 V")
    field(ELVL, "11")
#    field(TVST, "100 V")
#    field(TVVL, "12")
    field(SCAN, "I/O Intr")
}

record(bo, "$(P)$(R)Convert")
{
    field(PINI, "YES")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.CONVERT")
    field(ZNAM, "No")
    field(ONAM, "mV")
    field(VAL,  "0")
    info(autosaveFields, "VAL")
}

record(bi, "$(P)$(R)ConvertR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.CONVERT")
    field(ZNAM, "No")
    field(ONAM, "mV")
    field(SCAN, "I/O Intr")
}

# triggering

record(bo, "$(P)$(R)TrigUse")
{
    field(PINI, "YES")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.TRIG.USE")
    field(ZNAM, "No")
    field(ONAM, "Yes")
    field(VAL,  "0")
    info(autosaveFields, "VAL")
}

record(bi, "$(P)$(R)TrigUseR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.TRIG.USE")
    field(ZNAM, "No")
    field(ONAM, "Yes")
    field(SCAN, "I/O Intr")
}

# simple triggering (limited trigger configuration)

record(longout, "$(P)$(R)TrigSimpleThreshold")
{
    field(PINI, "YES")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.TRIG.SIMPLE.THRESHOLD")
    field(VAL,  "200")
    info(autosaveFields, "VAL")
}

record(longin, "$(P)$(R)TrigSimpleThresholdR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.TRIG.SIMPLE.THRESHOLD")
    field(SCAN, "I/O Intr")
}

record(longout, "$(P)$(R)TrigSimpleThresholdMv")
{
    field(PINI, "YES")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.TRIG.SIMPLE.THRESHOLD.MV")
    field(VAL,  "200")
    info(autosaveFields, "VAL")
}

record(longin, "$(P)$(R)TrigSimpleThresholdMvR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.TRIG.SIMPLE.THRESHOLD.MV")
    field(SCAN, "I/O Intr")
}

record(mbbo, "$(P)$(R)TrigSimpleDirection")
{
    field(PINI, "YES")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.TRIG.SIMPLE.DIRECTION")
    field(ZRST, "Above")
    field(ZRVL, "0")
    field(ONST, "Below")
    field(ONVL, "1")
    field(TWST, "Rising")
    field(TWVL, "2")
    field(THST, "Falling")
    field(THVL, "3")
    field(FRST, "Rising/Falling")
    field(FRVL, "4")
    field(VAL,  "0")
    info(autosaveFields, "VAL")
}

record(mbbi, "$(P)$(R)TrigSimpleDirectionR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.TRIG.SIMPLE.DIRECTION")
    field(ZRST, "Above")
    field(ZRVL, "0")
    field(ONST, "Below")
    field(ONVL, "1")
    field(TWST, "Rising")
    field(TWVL, "2")
    field(THST, "Falling")
    field(THVL, "3")
    field(FRST, "Rising/Falling")
    field(FRVL, "4")
    field(SCAN, "I/O Intr")
}

record(longout, "$(P)$(R)TrigSimpleDelay")
{
    field(PINI, "YES")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.TRIG.SIMPLE.DELAY")
    field(VAL,  "0")
    info(autosaveFields, "VAL")
}

record(longin, "$(P)$(R)TrigSimpleDelayR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.TRIG.SIMPLE.DELAY")
    field(SCAN, "I/O Intr")
}

record(longout, "$(P)$(R)TrigSimpleTimeout")
{
    field(PINI, "YES")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.TRIG.SIMPLE.TIMEOUT")
    field(VAL,  "0")
    info(autosaveFields, "VAL")
}

record(longin, "$(P)$(R)TrigSimpleTimeoutR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.TRIG.SIMPLE.TIMEOUT")
    field(SCAN, "I/O Intr")
}

# advanced triggering (complex trigger configuration)

record(mbbo, "$(P)$(R)TrigAdvancedCondition")
{
    field(PINI, "YES")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.TRIG.ADVANCED.CONDITION")
    field(ZRST, "Don't Care")
    field(ZRVL, "0")
    field(ONST, "True")
    field(ONVL, "1")
    field(TWST, "False")
    field(TWVL, "2")
    field(VAL,  "0")
    info(autosaveFields, "VAL")
}

record(mbbi, "$(P)$(R)TrigAdvancedConditionR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.TRIG.ADVANCED.CONDITION")
    field(ZRST, "Don't Care")
    field(ZRVL, "0")
    field(ONST, "True")
    field(ONVL, "1")
    field(TWST, "False")
    field(TWVL, "2")
    field(SCAN, "I/O Intr")
}

record(mbbo, "$(P)$(R)TrigAdvancedDirection")
{
    field(PINI, "YES")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.TRIG.ADVANCED.DIRECTION")
    # thresholds
    field(ZRST, "Above")
    field(ZRVL, "0")
    field(ONST, "Below")
    field(ONVL, "1")
    field(TWST, "Rising")
    field(TWVL, "2")
    field(THST, "Falling")
    field(THVL, "3")
    field(FRST, "Rising/Falling")
    field(FRVL, "4")
    field(FVST, "Above Lower")
    field(FVVL, "5")
    field(SXST, "Below Lower")
    field(SXVL, "6")
    field(SVST, "Rising Lower")
    field(SVVL, "7")
    field(EIST, "Falling Lower")
    field(EIVL, "8")
    # windowing
    field(NIST, "Inside")
    field(NIVL, "0")
    field(TEST, "Outside")
    field(TEVL, "1")
    field(ELST, "Enter")
    field(ELVL, "2")
    field(TVST, "Exit")
    field(TVVL, "3")
    field(TTST, "Enter/Exit")
    field(TTVL, "4")
    field(FTST, "Pos. Runt")
    field(FTVL, "9")
    field(FFST, "Neg. Runt")
    field(FFVL, "10")
    field(VAL,  "0")
    info(autosaveFields, "VAL")
}

record(mbbi, "$(P)$(R)TrigAdvancedDirectionR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.TRIG.ADVANCED.DIRECTION")
    # thresholds
    field(ZRST, "Above")
    field(ZRVL, "0")
    field(ONST, "Below")
    field(ONVL, "1")
    field(TWST, "Rising")
    field(TWVL, "2")
    field(THST, "Falling")
    field(THVL, "3")
    field(FRST, "Rising/Falling")
    field(FRVL, "4")
    field(FVST, "Above Lower")
    field(FVVL, "5")
    field(SXST, "Below Lower")
    field(SXVL, "6")
    field(SVST, "Rising Lower")
    field(SVVL, "7")
    field(EIST, "Falling Lower")
    field(EIVL, "8")
    # windowing
    field(NIST, "Inside")
    field(NIVL, "0")
    field(TEST, "Outside")
    field(TEVL, "1")
    field(ELST, "Enter")
    field(ELVL, "2")
    field(TVST, "Exit")
    field(TVVL, "3")
    field(TTST, "Enter/Exit")
    field(TTVL, "4")
    field(FTST, "Pos. Runt")
    field(FTVL, "9")
    field(FFST, "Neg. Runt")
    field(FFVL, "10")
    field(SCAN, "I/O Intr")
}

record(longout, "$(P)$(R)TrigAdvancedThresholdUpper")
{
    field(PINI, "YES")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.TRIG.ADVANCED.THRESHOLD.UPPER")
    field(VAL,  "200")
    info(autosaveFields, "VAL")
}

record(longin, "$(P)$(R)TrigAdvancedThresholdUpperR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.TRIG.ADVANCED.THRESHOLD.UPPER")
    field(SCAN, "I/O Intr")
}

record(longout, "$(P)$(R)TrigAdvancedThresholdUpperHyst")
{
    field(PINI, "YES")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.TRIG.ADVANCED.THRESHOLD.UPPER_HYSTERESIS")
    field(VAL,  "180")
    info(autosaveFields, "VAL")
}

record(longin, "$(P)$(R)TrigAdvancedThresholdUpperHystR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.TRIG.ADVANCED.THRESHOLD.UPPER_HYSTERESIS")
    field(SCAN, "I/O Intr")
}

record(longout, "$(P)$(R)TrigAdvancedThresholdLower")
{
    field(PINI, "YES")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.TRIG.ADVANCED.THRESHOLD.LOWER")
    field(VAL,  "180")
    info(autosaveFields, "VAL")
}

record(longin, "$(P)$(R)TrigAdvancedThresholdLowerR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.TRIG.ADVANCED.THRESHOLD.LOWER")
    field(SCAN, "I/O Intr")
}

record(longout, "$(P)$(R)TrigAdvancedThresholdLowerHyst")
{
    field(PINI, "YES")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.TRIG.ADVANCED.THRESHOLD.LOWER_HYSTERESIS")
    field(VAL,  "200")
    info(autosaveFields, "VAL")
}

record(longin, "$(P)$(R)TrigAdvancedThresholdLowerHystR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.TRIG.ADVANCED.THRESHOLD.LOWER_HYSTERESIS")
    field(SCAN, "I/O Intr")
}

record(bo, "$(P)$(R)TrigAdvancedThresholdMode")
{
    field(PINI, "YES")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.TRIG.ADVANCED.THRESHOLD.MODE")
    field(ZNAM, "Level")
    field(ONAM, "Window")
    field(VAL,  "0")
    info(autosaveFields, "VAL")
}

record(bi, "$(P)$(R)TrigAdvancedThresholdModeR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.TRIG.ADVANCED.THRESHOLD.MODE")
    field(ZNAM, "Level")
    field(ONAM, "Window")
    field(SCAN, "I/O Intr")
}

record(longout, "$(P)$(R)TrigAdvancedDelay")
{
    field(PINI, "YES")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.TRIG.ADVANCED.DELAY")
    field(VAL,  "0")
    info(autosaveFields, "VAL")
}

record(longin, "$(P)$(R)TrigAdvancedDelayR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.TRIG.ADVANCED.DELAY")
    field(SCAN, "I/O Intr")
}

record(longout, "$(P)$(R)TrigAdvancedTimeout")
{
    field(PINI, "YES")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.TRIG.ADVANCED.TIMEOUT")
    field(VAL,  "0")
    info(autosaveFields, "VAL")
}

record(longin, "$(P)$(R)TrigAdvancedTimeoutR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.TRIG.ADVANCED.TIMEOUT")
    field(SCAN, "I/O Intr")
}

# Pulse Width Qualifier

record(mbbo, "$(P)$(R)PulseWidthQualifierCondition")
{
    field(PINI, "YES")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.PWQ.CONDITION")
    field(ZRST, "Don't Care")
    field(ZRVL, "0")
    field(ONST, "True")
    field(ONVL, "1")
    field(TWST, "False")
    field(TWVL, "2")
    field(VAL,  "0")
    info(autosaveFields, "VAL")
}

record(mbbi, "$(P)$(R)PulseWidthQualifierConditionR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.PWQ.CONDITION")
    field(ZRST, "Don't Care")
    field(ZRVL, "0")
    field(ONST, "True")
    field(ONVL, "1")
    field(TWST, "False")
    field(TWVL, "2")
    field(SCAN, "I/O Intr")
}

record(mbbo, "$(P)$(R)PulseWidthQualifierDirection")
{
    field(PINI, "YES")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.PWQ.DIRECTION")
    # thresholds
    field(ZRST, "Above")
    field(ZRVL, "0")
    field(ONST, "Below")
    field(ONVL, "1")
    field(TWST, "Rising")
    field(TWVL, "2")
    field(THST, "Falling")
    field(THVL, "3")
    field(FRST, "Rising/Falling")
    field(FRVL, "4")
    field(FVST, "Above Lower")
    field(FVVL, "5")
    field(SXST, "Below Lower")
    field(SXVL, "6")
    field(SVST, "Rising Lower")
    field(SVVL, "7")
    field(EIST, "Falling Lower")
    field(EIVL, "8")
    # windowing
    field(NIST, "Inside")
    field(NIVL, "0")
    field(TEST, "Outside")
    field(TEVL, "1")
    field(ELST, "Enter")
    field(ELVL, "2")
    field(TVST, "Exit")
    field(TVVL, "3")
    field(TTST, "Enter/Exit")
    field(TTVL, "4")
    field(FTST, "Pos. Runt")
    field(FTVL, "9")
    field(FFST, "Neg. Runt")
    field(FFVL, "10")
    field(VAL,  "0")
    info(autosaveFields, "VAL")
}

record(mbbi, "$(P)$(R)PulseWidthQualifierDirectionR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.PWQ.DIRECTION")
    # thresholds
    field(ZRST, "Above")
    field(ZRVL, "0")
    field(ONST, "Below")
    field(ONVL, "1")
    field(TWST, "Rising")
    field(TWVL, "2")
    field(THST, "Falling")
    field(THVL, "3")
    field(FRST, "Rising/Falling")
    field(FRVL, "4")
    field(FVST, "Above Lower")
    field(FVVL, "5")
    field(SXST, "Below Lower")
    field(SXVL, "6")
    field(SVST, "Rising Lower")
    field(SVVL, "7")
    field(EIST, "Falling Lower")
    field(EIVL, "8")
    # windowing
    field(NIST, "Inside")
    field(NIVL, "0")
    field(TEST, "Outside")
    field(TEVL, "1")
    field(ELST, "Enter")
    field(ELVL, "2")
    field(TVST, "Exit")
    field(TVVL, "3")
    field(TTST, "Enter/Exit")
    field(TTVL, "4")
    field(FTST, "Pos. Runt")
    field(FTVL, "9")
    field(FFST, "Neg. Runt")
    field(FFVL, "10")
    field(SCAN, "I/O Intr")
}

record(longout, "$(P)$(R)PulseWidthQualifierLimitLower")
{
    field(PINI, "YES")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.PWQ.LIMIT.LOWER")
    field(VAL,  "10")
    info(autosaveFields, "VAL")
}

record(longin, "$(P)$(R)PulseWidthQualifierLimitLowerR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.PWQ.LIMIT.LOWER")
    field(SCAN, "I/O Intr")
}

record(longout, "$(P)$(R)PulseWidthQualifierLimitUpper")
{
    field(PINI, "YES")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.PWQ.LIMIT.UPPER")
    field(VAL,  "50")
    info(autosaveFields, "VAL")
}

record(longin, "$(P)$(R)PulseWidthQualifierLimitUpperR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.PWQ.LIMIT.UPPER")
    field(SCAN, "I/O Intr")
}

record(mbbo, "$(P)$(R)PulseWidthQualifierType")
{
    field(PINI, "YES")
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.PWQ.TYPE")
    field(ZRST, "None")
    field(ZRVL, "0")
    field(ONST, "Less Than")
    field(ONVL, "1")
    field(TWST, "More Than")
    field(TWVL, "2")
    field(THST, "In Range")
    field(THVL, "3")
    field(FRST, "Out Of Range")
    field(FRVL, "4")
    field(VAL,  "0")
    info(autosaveFields, "VAL")
}

record(mbbi, "$(P)$(R)PulseWidthQualifierTypeR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))PS4000A.CH.PWQ.TYPE")
    field(ZRST, "None")
    field(ZRVL, "0")
    field(ONST, "Less Than")
    field(ONVL, "1")
    field(TWST, "More Than")
    field(TWVL, "2")
    field(THST, "In Range")
    field(THVL, "3")
    field(FRST, "Out Of Range")
    field(FRVL, "4")
    field(SCAN, "I/O Intr")
}
