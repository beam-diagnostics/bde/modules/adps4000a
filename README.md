# Pico Technologies PicoScope 4000A Series


# Startup

	# Create a PicoScope 4000A driver
	# PS4000AConfig(const char *portName, int numSamples, int dataType,
	#               int maxBuffers, int maxMemory, int priority, int stackSize)
	# dataType == NDUInt32 == 5
	PS4000AConfig("PICO1", 1000, 5, 0, 0)

	PicoScope 4000 Series Driver
	checkForDevice: opening the device...
	checkForDevice: ps4000OpenUnit() returned 0
	checkForDevice: got handle 1
	checkForDevice: device opened successfully!
	Driver Version: PS4000A Linux Driver, 2.1.0.570
	USB Version: 3.0
	Hardware Version: 1
	Variant Info: 4824
	Serial: GO036/009
	Cal Date: 18Sep18
	Kernel Version: 0.0
	Digital HW Version: 1
	Analogue HW Version: 1
	Firmware 1: 1.7.5.0
	Firmware 2: 1.0.11.0
